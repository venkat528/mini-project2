package com.hcl.miniproject.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.hcl.miniproject.model.Customer;

public interface CustomerService {
	List<Customer> getAllCustomers();
	void saveCustomer(Customer cust);
	Customer getCustomerById(long id);
	void deleteCustomerById(long id);
	Page<Customer> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
